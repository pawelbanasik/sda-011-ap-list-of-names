package com.pawelbanasik.sda_011_ap_list_of_names;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // to zadanie polega na zabawie plikiem strings.xml oraz
    // plikiem activity_main.xml zeby nauczyc sie nie uzywania designera
    // oraz pokazywał jak pracowac na osobnych galeziach uzywajac GIT
}
